# Story Perancangan & Pemrograman Web Fasilkom UI 2018

##	Nama
Annisaa Kayza Adisti (1706025106)
PPW - D

## Herokuapp Link
https://kayza-story6.herokuapp.com

## Website Status
[![pipeline status](https://gitlab.com/kayzaadisti/story_6/badges/master/pipeline.svg)](https://gitlab.com/kayzaadisti/story_6/commits/master)

[![coverage report](https://gitlab.com/kayzaadisti/story_6/badges/master/coverage.svg)](https://gitlab.com/kayzaadisti/story_6/commits/master)
