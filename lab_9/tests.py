from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import bookList, JSonSearch, tambah, kurang
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import auth

class Lab9UnitTest(TestCase):
	def test_lab_9_list_book_url_is_exist(self):
		response = Client().get('/books')
		self.assertEqual(response.status_code, 200)

	def test_lab_9_json_file_is_exist(self):
		response = Client().get('/searchJson?cari=')
		self.assertEqual(response.status_code, 200)

	def test_lab_9_using_books_template(self):
		response = Client().get('/books')
		self.assertTemplateUsed(response, 'books.html')

	def test_lab_11_using_tambah(self):
		found = resolve('/tambah')
		self.assertEqual(found.func, tambah)

	def test_lab_11_using_kurang(self):
		found = resolve('/kurang')
		self.assertEqual(found.func, kurang)

	



