"""lab_6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.urls import re_path, include
from django.conf.urls import url, include
from django.conf.urls.static import static
from appstory6.views import status as bikinStatus
from django.contrib.auth import views
from .views import logoutPage

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('appstory6.urls')),
    path('', include('lab_9.urls')),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', logoutPage, name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),
    
]

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()
