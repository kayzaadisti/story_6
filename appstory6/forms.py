from django import forms
from .models import Status, Subscribe

class Status_Form(forms.Form):
	isi_status = forms.CharField(label="Enter your Status ", required=True)

class SubscriberForm(forms.Form):
	name_attrs = {
		'type':'text',
		'class':'form-control',
		'placeholder': 'Full Name',
	}

	mail_attrs = {
		'type':'text',
		'class':'form-control',
		'placeholder':'Example: kayza@mail.com'
	}

	pass_attrs = {
		'type':'password',
		'class':'form-control',
		'placeholder':'Password',
	}

	name = forms.CharField(max_length=100, error_messages={"required":"Please enter your name"}, widget= forms.TextInput(attrs=name_attrs))
	password = forms.CharField(max_length=20, min_length=6, error_messages={"min_length":"Password must be longer than 5 characters"}, widget= forms.PasswordInput(attrs=pass_attrs))
	email = forms.EmailField(max_length=50, error_messages={"required":"Please enter a valid email"}, widget=forms.TextInput(attrs=mail_attrs))



