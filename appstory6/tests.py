from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import Status, Subscribe
from .forms import Status_Form, SubscriberForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from django.utils.encoding import force_text

# Create your tests here.

#------Unit Tests------#
class Lab6UnitTest(TestCase):
	# ngecek url ada apa engga 
	def test_lab_6_status_page_url_is_exist(self):
		response = Client().get('/status')
		self.assertEqual(response.status_code, 200)

	def test_lab_6_profile_page_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_lab6_has_this_text(self):
		response = Client().get('/status')
		text = response.content.decode('utf8')
		self.assertIn("Hello, Apa kabar?", text)

	def test_lab6_has_name(self):
		response = Client().get('/')
		text = response.content.decode('utf8')
		self.assertIn("Annisaa Kayza Adisti", text)

	def test_lab6_has_npm(self):
		response = Client().get('/')
		text = response.content.decode('utf8')
		self.assertIn("1706025106", text)

	def test_lab6_has_birth_date(self):
		response = Client().get('/')
		text = response.content.decode('utf8')
		self.assertIn("Born in Jakarta, November 16 1999", text)

	def test_lab6_has_hobby(self):
		response = Client().get('/')
		text = response.content.decode('utf8')
		self.assertIn("Movies and Dramas enthusiast", text)

	# menggunakan fungsi status
	def test_lab_6_use_status(self):
		found = resolve('/status')
		self.assertEqual(found.func, status)

	def test_lab_6_use_profile(self):
		found = resolve('/')
		self.assertEqual(found.func, profile)

	# bisa menambah status baru	
	def test_lab_6_can_create_new_status(self):
		# creating a new status
		new_status = Status.objects.create(isi_status='halo')

		# retrieving all available status
		count_all_available_status = Status.objects.all().count()
		self.assertEqual(count_all_available_status, 1)

	# kalo blank formnya
	def test_form_validation_for_blank_items(self):
		form = Status_Form(data={'isi_status': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['isi_status'], ["This field is required."])

	# test apakah sukses buat ngerender
	def test_lab6_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/status', {'isi_status': test})
		self.assertEqual(response_post.status_code, 302)

		response = Client().get('/status')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	# kalo error 
	def test_lab6_post_error_and_render_the_result(self):
		test = 'Kayza belajar ppw'
		response_post = Client().post('/status', {'isi_status': test})
		self.assertEqual(response_post.status_code, 302)

		response = Client().get('/status')
		html_response = response.content.decode('utf8')
		self.assertNotIn('kayza belajar sda', html_response)

	def test_lab8_has_about_me(self):
		response = Client().get('/')
		text = response.content.decode('utf8')
		self.assertIn("About Me", text)

	def test_lab8_has_activity(self):
		response = Client().get('/')
		text = response.content.decode('utf8')
		self.assertIn("Activity", text)

	def test_lab8_has_pengalaman(self):
		response = Client().get('/')
		text = response.content.decode('utf8')
		self.assertIn("Experiences", text)

	def test_lab8_has_prestasi(self):
		response = Client().get('/')
		text = response.content.decode('utf8')
		self.assertIn("Achievements", text)


	def test_lab10_subscribe_url(self):
		response = Client().get('/subscribe')
		self.assertEqual(response.status_code, 200)

	def test_lab10_subscribe_using_func(self):
		found = resolve('/subscribe')
		self.assertEqual(found.func, subscribe)

	def test_lab10_subscribe_can_create_obj(self):
		Subscribe.objects.create(name="kayza", password="kucing", email="yuhuyuyuy@gmail.com")
		count_all_subscriber = Subscribe.objects.all().count()
		self.assertEqual(count_all_subscriber, 1)

	def test_lab10_subscribe_form_has_placeholder_and_css(self):
		form = SubscriberForm()
		self.assertIn('class="form-control"', form.as_p())

	def test_lab10_subscribe_form_validation_for_blank_items(self):
		form = SubscriberForm(data={'name':'', 'password':'', 'email':''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['name'],['Please enter your name']
		)
		self.assertEqual(
			form.errors['email'],['Please enter a valid email']
		)
		self.assertEqual(
			form.errors['password'],['This field is required.']
		)

	def test_lab10_subscribe_pass_less_than_6char(self):
		form = SubscriberForm(data={'name':'kayza', 'password':'kuci', 'email':'yuhuyuyuy@gmail.com'})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['password'],["Password must be longer than 5 characters"]
		)

	def test_lab10_subscribe_email_has_exist(self):
		nama = 'kayza'
		password = 'kucing'
		email = 'yuhuyuyuy@gmail.com'
		postIt = Client().post('/validate', {'email':email})
		# respond it false
		self.assertJSONEqual(force_text(postIt.content), {'not_valid': False})
		# send data to database successfully
		Client().post('/success', {'name': nama, 'password': password, 'email':email})
		count_subscriber = Subscribe.objects.all().count()
		self.assertEqual(count_subscriber, 1)
		# enter the same email for the second time
		postIt = Client().post('/validate', {'email': email})
		# email already taken so not_valid = True
		self.assertJSONEqual(str(postIt.content, encoding='utf8'), {'not_valid': True})
		# the not valid data is not entered into database
		count_subscriber = Subscribe.objects.all().count()
		self.assertEqual(count_subscriber, 1)
#------Functional Tests------#
class Lab7FunctionalTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Lab7FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Lab7FunctionalTest, self).tearDown()

	# def test_input_status(self):
	# 	selenium = self.selenium
	# 	# open localhost
	# 	selenium.get('http://127.0.0.1:8000/status')
	# 	# find the form element that you wanted to test
	# 	stat = selenium.find_element_by_id('id_isi_status')
	# 	# Fill the status form with status you want to post
	# 	stat.send_keys('Coba Coba')
	# 	# submit it
	# 	stat.send_keys(Keys.RETURN)
	# 	check = selenium.find_elements_by_id('stat')
	# 	self.assertIn('Coba Coba', check[-1].text)
	# 	time.sleep(5)

	def test_accordion(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		acc = selenium.page_source
		self.assertIn("About Me", acc)

	# def test_accordion_toggle(self):
	# 	selenium = self.selenium
	# 	selenium.get('http://127.0.0.1:8000/')
	# 	acc_button = selenium.find_element_by_class_name('accordion-toggle')
	# 	acc_button.click()
	# 	time.sleep(3)

	# def test_change_theme(self):
	# 	selenium = self.selenium
	# 	selenium.get('http://127.0.0.1:8000/')
	# 	theme_button = selenium.find_element_by_class_name('btn-primary')
	# 	theme_button.click()
	# 	time.sleep(3)

	def test_layout_profile(self):
		selenium = self.selenium
		selenium.get('http://kayza-story6.herokuapp.com/')
		header_text = selenium.find_element_by_tag_name('h1').text
		self.assertIn('My Profile', header_text)
	
		p_text = selenium.find_element_by_tag_name('p').text
		self.assertIn('Annisaa Kayza Adisti\n1706025106\nBorn in Jakarta, November 16 1999\nMovies and Dramas enthusiast', p_text)
		time.sleep(3)

	def test_css_is_correct(self):
		selenium = self.selenium
		selenium.get('http://kayza-story6.herokuapp.com/status')
		row_inside_css = selenium.find_element_by_css_selector('div.col-md-12')
		row_outside_css = selenium.find_element_by_css_selector('div.col-md-6')

		property_of_inside_css = row_inside_css.value_of_css_property('border-radius')
		property_of_inside_css2 = row_inside_css.value_of_css_property('margin-top')

		self.assertIn('class="col-md-12"', selenium.page_source)
		self.assertIn('class="col-md-6"', selenium.page_source)
		self.assertIn('36px', property_of_inside_css)
		self.assertIn('20px', property_of_inside_css2)

		time.sleep(2)




















