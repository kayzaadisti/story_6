(function ($) {
    'use strict';

    var browserWindow = $(window);

    // :: 1.0 Preloader Active Code
    browserWindow.on('load', function () {
        $('#preloader').fadeOut('slow', function () {
            $(this).remove();
        });
    });

   
})(jQuery);


var color = ["rgba(32,178,170,0.7)", "rgba(100,149,237,0.7)", "rgba(221,160,221,0.7)", "rgba(255,153,204, 0.7)"]
var color2 = ["rgba(32,178,170,0.7)", "rgba(100,149,237,0.7)", "rgba(255,153,204, 0.7)",]
var i = 0;
document.querySelector("button").addEventListener("click", function(){
    i= i < color.length ? ++i :0;
    document.querySelector(".col-sm-12").style.backgroundColor = color[i]
    document.querySelector(".head-accordion").style.backgroundColor = color2[i]
})

$(document).ready(function($) {
    $('.accordion').find('.accordion-toggle').click(function(){

      //Expand or collapse this panel
      $(this).next().slideToggle('fast');

      //Hide the other panels
      $(".accordion-content").not($(this).next()).slideUp('fast');

    });
});