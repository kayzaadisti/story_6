from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .models import Status, Subscribe
from .forms import Status_Form, SubscriberForm
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.forms.models import model_to_dict


def status(request):
	form = Status_Form(request.POST or None)
	response = {}
	if(request.method == "POST"):
		if(form.is_valid()):
			isi_status = request.POST.get("isi_status")
			Status.objects.create(isi_status = isi_status,)
			return redirect('/')
		else:
			return render(request, 'statusInput.html', response)
	else:
		isi_status = Status.objects.all()
		response['form'] = form
		response['isi_status'] = isi_status
		return render(request, 'statusInput.html', response)

def profile(request):
	return render(request, 'checkProfile.html')

def subscribe(request):
	response = {}
	response["forms"] = SubscriberForm()
	return render(request, "subscribe.html", response)

@csrf_exempt
def checkValid(request):
	email = request.POST.get("email")
	data = {'not_valid': Subscribe.objects.filter(email__iexact=email).exists()}
	return JsonResponse(data)

def success(request):
	submit_form = SubscriberForm(request.POST or None)
	if(submit_form.is_valid()):
		cd = submit_form.cleaned_data
		newSubscriber = Subscribe(name=cd['name'], password=cd['password'], email=cd['email'])
		newSubscriber.save()
		data = {'name': cd['name'], 'password': cd['password'], 'email':cd['email']}
	return JsonResponse(data)

def getSubscribers(request):
	jsonData = serializers.serialize('json', Subscribe.objects.all())
	return HttpResponse(jsonData, content_type='application/json')

def subscribers(request):
	return render(request, 'subscribers.html')

@csrf_exempt
def deleteSubscribers(request):
	if 'email' in request.POST:
		print('bismillah')
		email = request.POST['email']
		delsub = Subscribe.objects.filter(email=email)
		delsub.delete()
		return HttpResponseRedirect("/subscribers")
	else:
		print('lalala')
		return HttpResponseRedirect("/subscribers")

