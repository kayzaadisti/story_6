from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.core import serializers
import requests
import json

def bookList(request):
	if request.user.is_authenticated:
		request.session['first_name'] = request.user.first_name
		request.session['last_name'] = request.user.last_name
		request.session['email'] = request.user.email
		request.session.get('counter', 0)
		for key, value in request.session.items():
			print('{} => {}'.format(key, value))
	return render(request, 'books.html') 
def JSonSearch(request):
	searching =  request.GET.get('cari')	
	url = "https://www.googleapis.com/books/v1/volumes?q=" + searching
	data = requests.get(url).json()
	if request.user.is_authenticated:
		count = request.session.get('counter', 0)
		request.session['counter'] = count
		for key, value in request.session.items():
			print('{} => {}'.format(key, value))
	return JsonResponse(data)

def tambah(request):
	request.session['counter'] = request.session['counter'] + 1
	return HttpResponse(request.session['counter'], content_type = "application/json")

def kurang(request):
	request.session['counter'] = request.session['counter'] - 1
	return HttpResponse(request.session['counter'], content_type = "application/json")


