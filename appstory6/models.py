from django.db import models


class Status(models.Model):
	isi_status = models.CharField(max_length=300)

class Subscribe(models.Model):
	name = models.CharField(max_length=100)
	password = models.CharField(max_length=20, unique=True)
	email = models.CharField(max_length=50)

# Create your models here.

