$(document).ready(function(){
	var name = "";
	var password = "";
	$("#id_name").change(function(){
		name = $(this).val();
	})
	$("#id_password").change(function(){
		password = $(this).val();
	})
	$("#id_email").change(function(){
		var email = $(this).val();
		console.log(email)
		$.ajax({
			url: "/validate",
			data:{'email':email,},
			method: "POST",
			dataType: 'json',
			success: function(data){
				console.log(data)
				if(data.not_valid){
					alert("This email has already been taken");
				}
				else{
					if(name.length > 0 && password.length > 5){
						$("#submit_button").removeAttr("disabled");
					}
				}
			}
		});
	});
	$("#submit_button").click(function(){
		event.preventDefault(); 
		$.ajax({
			headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
			url: "/success",
			data: $("form").serialize(),
			method: 'POST',
			dataType: 'json',
			success: function(data){
				swal({
					title: "Welcome " + data.name,
					text: "Thanks for subscribing Kayza!",
					type: 'success',
					showConfirmButton: false,
					timer: 2500
				}).then(function(){
					window.location = "/"
				});
			}, error: function(data){
				console.log(data);
				swal({
					type: 'error',
					title: 'Sorry',
					text: 'Something is wrong! Please try again',
				}).then(function(){
					window.location = "/subscribe"
				});
			}
		})
	});
});