from django.urls import path
from django.contrib import admin
from . import views 
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from .views import *

urlpatterns = [
	path('', views.profile, name="profile"),
    path('status', views.status, name="status"),
    path('subscribe', views.subscribe, name="subscribe"),
    path('validate', views.checkValid, name='validate'),
    path('success', views.success, name="success"),
    path('subscribers', views.subscribers, name='subscribers'),
    path('allSubscribers', views.getSubscribers, name='allSubscribers'),
    path('deleteSubscribers', views.deleteSubscribers, name='deleteSubscribers'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)